package com.soaint.lineabase.commons.converter;

import com.soaint.lineabase.commons.domains.generic.PersonaDTO;
import com.soaint.lineabase.commons.domains.response.BaseResponse;
import com.soaint.lineabase.commons.util.JSONUtil;
import org.springframework.http.ResponseEntity;

public class PersonaConverter {

    /**
     * Metodo para convertir un ResponseEntity en un objeto de tipo ClientPersonaDTO
     *
     * @param responseEntity respuesta de un cliente
     * @return objeto de tipo ClientPersonaDTO
     */
    public static PersonaDTO clientToPersonaDto(ResponseEntity<BaseResponse> responseEntity) {
        String body = JSONUtil.marshal(responseEntity.getBody().getBody());
        return JSONUtil.unmarshal(body, PersonaDTO.class);
    }


}
