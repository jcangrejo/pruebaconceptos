package com.soaint.lineabase.commons.constants.api.persona;

public interface EndpointCliente {

    String CLIENTES = "/";
    String FIND_PERSONAS_BY_ID = "/{id}";
    String UPDATE_PERSONAS_BY_ID = "/{id}";
    String CREATE_PERSONA = "/";
}
