package com.soaint.lineabase.commons.constants.api.persona;

public interface EndpointPersonaApi {

    String PERSONA_API_V1 = "person/client";
    String FIND_PERSONAS = "/";
    String FIND_PERSONAS_BY_ID = "/{id}";
    String UPDATE_PERSONAS_BY_ID = "/{id}";
    String CREATE_PERSONA = "/";
    String DELETE_PERSONA_BY_ID = "/{id}";
}
