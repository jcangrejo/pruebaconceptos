package com.soaint.lineabase.commons.domains.response;

import com.soaint.lineabase.commons.domains.generic.PersonaDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class PersonaDTOResponse extends PersonaDTO {



}
