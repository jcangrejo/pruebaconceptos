package com.soaint.lineabase.commons.exception.business;

import com.soaint.lineabase.commons.exception.generic.BaseRuntimeException;

public class NoCreatedException extends BaseRuntimeException {

    public NoCreatedException() {
    }
}
