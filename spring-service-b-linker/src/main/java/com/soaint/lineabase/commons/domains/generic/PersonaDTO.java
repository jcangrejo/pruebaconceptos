package com.soaint.lineabase.commons.domains.generic;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class PersonaDTO  implements Serializable {

    private Long id;
    private String tipoIdentificacion;
    private Long numeroIdentificacion;
    private String nombres;
    private String apellidos;
    private Date createDate;

}
