package com.soaint.lineabase.commons.exception.business;

import com.soaint.lineabase.commons.exception.generic.BaseRuntimeException;

public class ResponseNotFoundException extends BaseRuntimeException {
    public ResponseNotFoundException() {
    }
}
