package com.soaint.lineabase.web.api.rest;

import com.soaint.lineabase.commons.domains.request.PersonaDTORequest;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface ImplementadorApi {

    ResponseEntity createPersona(PersonaDTORequest persona, HttpServletRequest request);

    ResponseEntity findPersonas(HttpServletRequest request);

    ResponseEntity findPersonasById(Long id, HttpServletRequest request);

    ResponseEntity updatePersonasById(Long id, PersonaDTORequest persona);

    ResponseEntity deletePersonaById(Long id, HttpServletRequest request);

}
