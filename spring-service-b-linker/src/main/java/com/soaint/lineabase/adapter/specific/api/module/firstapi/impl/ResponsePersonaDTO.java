package com.soaint.lineabase.adapter.specific.api.module.firstapi.impl;


import com.soaint.lineabase.commons.domains.response.PersonaDTOResponse;
import lombok.Data;

import java.io.Serializable;

@Data
public class ResponsePersonaDTO implements Serializable {

    private PersonaDTOResponse body;
    private String status;
    private String timeResponse;
    private String message;
    private String path;
    private String transactionState;



}
