package com.soaint.lineabase.adapter.specific.api.module.firstapi;

import com.soaint.lineabase.commons.domains.generic.PersonaDTO;
import com.soaint.lineabase.commons.domains.request.PersonaDTORequest;

import java.util.List;

public interface PersonaApiCliente {

    PersonaDTO create(PersonaDTORequest persona);

    List<PersonaDTO> personas();

    PersonaDTO findPersonaById(Long id);

    PersonaDTO updatePersonaById(Long id, PersonaDTORequest persona);

    String deletePersonabyId(Long id);

}
