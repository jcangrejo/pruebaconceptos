package com.soaint.lineabase.commons.converter;

import com.soaint.lineabase.commons.domains.generic.PersonaDTO;
import com.soaint.lineabase.model.entities.Persona;

import java.util.Optional;

public class PersonaConverter {


    public static PersonaDTO entityToDtoPersona(Optional<Persona> persona) {
        PersonaDTO personaDTO = new PersonaDTO();
        if (persona.isPresent()) {
            personaDTO.setId(persona.get().getId());
            personaDTO.setNombres(persona.get().getNombres());
            personaDTO.setApellidos(persona.get().getApellidos());
            personaDTO.setNumeroIdentificacion(persona.get().getNumeroIdentificacion());
            personaDTO.setTipoIdentificacion(persona.get().getTipoIdentificacion());
            personaDTO.setCreateDate(persona.get().getCreateDate());
        }
        return personaDTO;
    }


}
