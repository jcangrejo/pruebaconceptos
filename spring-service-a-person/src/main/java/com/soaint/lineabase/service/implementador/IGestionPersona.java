package com.soaint.lineabase.service.implementador;

import com.soaint.lineabase.commons.domains.generic.PersonaDTO;
import com.soaint.lineabase.commons.domains.request.PersonaDTORequest;

import java.util.Collection;
import java.util.Optional;

public interface IGestionPersona {

    Optional<PersonaDTO> registerPersona(final PersonaDTORequest persona);

    Optional<Collection<PersonaDTO>> findPersonas();

    PersonaDTO getPersonaById(final Long id);

    Optional<PersonaDTO> updatePersona(final PersonaDTORequest persona, final Long id);

    Optional<String> detelePersona(final Long id);



}
